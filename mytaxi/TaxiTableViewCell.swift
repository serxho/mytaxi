//
//  TaxiTableViewCell.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import UIKit

class TaxiTableViewCell: UITableViewCell {

    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func completeCellWithModel(taxiModel: TaxiModel) {
        typeLabel.text = taxiModel.type + ": \(taxiModel.id)"
        let taxiState = taxiModel.state
        if taxiState == "ACTIVE" {
            stateLabel.textColor = UIColor.green
        } else {
            stateLabel.textColor = UIColor.red
        }
        stateLabel.text = taxiState
    }
    
}
