//
//  TaxiListViewController.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import UIKit
import MapKit

class TaxiListViewController: UIViewController {
    
    var taxiList: [TaxiModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        getTaxiList()
        
        showMapsButton()
        showFilterButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupTableView() {
        
        title = "mytaxi"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "TaxiTableViewCell", bundle: nil), forCellReuseIdentifier: "TaxiTableViewCell")
        
        tableView.tableFooterView = UIView()
    }
    
    func getTaxiList() {
        
        let p1 = CLLocationCoordinate2D(latitude: 53.694865, longitude: 9.757589)
        let p2 = CLLocationCoordinate2D(latitude: 53.394655, longitude: 10.099891)
        
        DataManager.sharedInstance.getTaxiList(p1: p1, p2: p2) { (success, taxiResponse) in
            if success {
                if taxiResponse != nil {
                   self.taxiList = taxiResponse!.poiList
                }
            }
        }
    }
    
    func openInMaps() {
        let mapViewController = MapViewController(nibName: "MapViewController", bundle: nil)
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }
    
    func filterActiveTaxies() {
        let allButton = UIBarButtonItem(title: "All", style: .plain, target: self, action: #selector(TaxiListViewController.showAllTaxies))
        navigationItem.leftBarButtonItem = allButton
        
        taxiList = taxiList.filter({$0.state == "ACTIVE"})
        
    }
    
    func showAllTaxies() {
        showFilterButton()
        getTaxiList()
    }
    
    func showFilterButton() {
        let filterButton = UIBarButtonItem(title: "Active", style: .plain, target: self, action: #selector(TaxiListViewController.filterActiveTaxies))
        navigationItem.leftBarButtonItem = filterButton
    }
    
    func showMapsButton() {
        let mapsButton = UIBarButtonItem(title: "Map", style: .plain, target: self, action: #selector(TaxiListViewController.openInMaps))
        navigationItem.rightBarButtonItem = mapsButton
    }
    
}

extension TaxiListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taxiList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taxiCell = tableView.dequeueReusableCell(withIdentifier: "TaxiTableViewCell") as! TaxiTableViewCell
        taxiCell.completeCellWithModel(taxiModel: taxiList[indexPath.row])
        return taxiCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
