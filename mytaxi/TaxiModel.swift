//
//  TaxiModel.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import Foundation
import ObjectMapper

class TaxiResponse: Mappable {
    
    var poiList: [TaxiModel] = []
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        poiList                          <- map["poiList"]
    }
}

class TaxiModel: Mappable {
    var id: Int = 0
    var coordinate: Coordinate = Coordinate()
    var state: String = ""
    var type: String = ""
    var heading: Double = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id                          <- map["id"]
        coordinate                  <- map["coordinate"]
        state                       <- map["state"]
        type                        <- map["type"]
        heading                     <- map["heading"]
    }
}

class Coordinate: Mappable {
    var latitude: Double = 0
    var longitude: Double = 0
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        latitude                        <- map["latitude"]
        longitude                       <- map["longitude"]
    }
}
