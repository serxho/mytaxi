//
//  RESTManager.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RestManager {
    
    static let sharedInstance = RestManager()
    private let manager: SessionManager
    
    init() {
        
        // Alamofire request configuration
        let configuration = URLSessionConfiguration.default
        manager = SessionManager(configuration: configuration)
        
    }
    
    func GET(url: String, completion: @escaping (_ jsonResponse: JSON?, _ statusCode: Int) -> ()) {
        print("GET Url: \(url)")
        
        self.manager
            .request(url, method: .get, encoding: JSONEncoding(options: []))
            .responseJSON { (response) in
                print(response)
                
                if let jsonResponse = response.result.value {
                    let json = JSON(jsonResponse as AnyObject)
                    completion(json, response.response?.statusCode ?? 0)
                } else {
                    completion(nil, response.response?.statusCode ?? 0)
                }
                
        }
    }
    
}
