//
//  Constants.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import Foundation

struct Config {
    static let baseURL = "https://poi-api.mytaxi.com/PoiService/poi/v1?"
}
