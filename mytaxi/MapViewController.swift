//
//  MapViewController.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var taxiList: [TaxiModel] = [] {
        didSet {
            for eachTaxi in taxiList {
                let annotation = MKPointAnnotation()
                annotation.title = eachTaxi.type
                annotation.subtitle = eachTaxi.state
                annotation.coordinate = CLLocationCoordinate2D(latitude: eachTaxi.coordinate.latitude, longitude: eachTaxi.coordinate.longitude)
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setupMapView() {
        mapView.delegate = self
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        mapView.removeAnnotations(mapView.annotations)
        
        let northEast = mapView.convert(CGPoint(x: mapView.bounds.width, y: 0), toCoordinateFrom: mapView)
        let southWest = mapView.convert(CGPoint(x: 0, y: mapView.bounds.height), toCoordinateFrom: mapView)
        
        getTaxiList(p1: northEast, p2: southWest)
    }
    
    func getTaxiList(p1: CLLocationCoordinate2D, p2: CLLocationCoordinate2D) {
        DataManager.sharedInstance.getTaxiList(p1: p1, p2: p2) { (success, taxiResponse) in
            if success {
                if taxiResponse != nil {
                    self.taxiList = taxiResponse!.poiList
                }
            }
        }
    }
    
}
