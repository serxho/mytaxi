//
//  DataManager.swift
//  mytaxi
//
//  Created by Serxho on 9/10/17.
//  Copyright © 2017 Serxho. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

import MapKit
class DataManager {
    
    static let sharedInstance = DataManager()
    
    init() {
        
    }
    
    func getTaxiList(p1: CLLocationCoordinate2D, p2: CLLocationCoordinate2D, completion: @escaping (_ success: Bool, _ taxiResponse: TaxiResponse?) -> ()) {
        
        let url = Config.baseURL + "p2Lat=\(p2.latitude)&p1Lon=\(p1.longitude)&p1Lat=\(p1.latitude)&p2Lon=\(p2.longitude)"
//        "p2Lat=53.394655&p1Lon=9.757589&p1Lat=53.694865&p2Lon=10.099891"
        
        RestManager.sharedInstance.GET(url: url) { (jsonResponse, statusCode) in
            
            if jsonResponse != nil {
                let taxiResponse = self.parseTaxiResponse(json: jsonResponse!)
                completion(true, taxiResponse)
            } else {
                completion(false, nil)
            }
        }
    }
    
    func parseTaxiResponse(json: JSON) -> TaxiResponse? {
        if let taxiResponse = json.dictionaryObject {
            return Mapper<TaxiResponse>().map(JSON: taxiResponse)
        } else {
            return nil
        }
    }
}
